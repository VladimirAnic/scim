package com.eviden.evidianng.scimproxy.controller;

import com.eviden.evidianng.scimproxy.service.HWService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello-world")
@RequiredArgsConstructor
public class HelloWorldController {

    private final HWService hwService;

    @GetMapping
    public String helloWorld() {
        return hwService.hello();
    }
}
