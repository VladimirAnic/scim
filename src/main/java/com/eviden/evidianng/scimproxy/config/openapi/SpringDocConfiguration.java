package com.eviden.evidianng.scimproxy.config.openapi;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for setting up Swagger UI interface.
 */
@Configuration
public class SpringDocConfiguration {

    private static final String OPENAPI_TITLE = "SCIM API proxy";
    private static final String OPENAPI_DESCRIPTION = "SCIM API proxy documentation";
    private static final String OPENAPI_VERSION = "1.0";

    /**
     * Spring Bean for OpenApi swagger.
     */
    @Bean
    public OpenAPI springdocOpenAPI() {
        return new OpenAPI()
                .info(new Info().title(OPENAPI_TITLE)
                        .description(OPENAPI_DESCRIPTION)
                        .version(OPENAPI_VERSION));
    }
}
