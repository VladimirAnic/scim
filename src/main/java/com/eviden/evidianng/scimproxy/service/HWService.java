package com.eviden.evidianng.scimproxy.service;

import io.micrometer.observation.annotation.Observed;
import org.springframework.stereotype.Service;

@Observed(name = "hwService")
@Service
public class HWService {

    public String hello() {
        return "hello";
    }
}
