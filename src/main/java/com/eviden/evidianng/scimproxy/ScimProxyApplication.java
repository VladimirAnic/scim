package com.eviden.evidianng.scimproxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScimProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScimProxyApplication.class, args);
    }

}
